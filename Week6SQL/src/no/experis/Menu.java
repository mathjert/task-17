package no.experis;

import javafx.util.Pair;
import no.experis.model.Email;
import no.experis.model.Person;
import no.experis.model.PhoneNumber;
import no.experis.model.RelationshipTypes;
import no.experis.querys.*;

import java.util.ArrayList;
import java.util.Scanner;

class Menu {
    private Scanner userInput;


    public void printMenu() throws Exception {
        try {
            userInput = new Scanner(System.in);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        String opr = mainMenu(userInput);

        if (opr.equalsIgnoreCase("1")) {
            String res = searchMenu(userInput);
            SearchQueryHandler sqh = new SearchQueryHandler();
            ArrayList<Integer> IDs = sqh.search(res);
           /* ArrayList<Integer> IDs2 = sqh.searchNameOnly(res);
            for(int id : IDs2){
                if(!IDs.contains(id))
                    IDs.add(id);
            }*/
            GenerateOutput go = new GenerateOutput();
            ArrayList<Person> people = go.generateMatchList(IDs);

            for (Person p : people) {
                System.out.println(p.toString() + "\n");
            }
        }
        if (opr.equalsIgnoreCase("2")) {
            String resOpr = crudOprMenu(userInput);
            String resTable = crudMenu(userInput);
            ArrayList<String> res = crudMenu2(userInput, Integer.parseInt(resOpr), Integer.parseInt(resTable));
            if (res.isEmpty() && !resOpr.equalsIgnoreCase("2")) {
                System.out.println("Empty list");
                return;
            }
            handleCrud(Integer.parseInt(resOpr), Integer.parseInt(resTable), res);
        }
    }


    public void handleCrud(int opr, int table, ArrayList<String> variables) {
        boolean res = false;
        String resultat = "";
        try {
            if (table == 1) {
                PersonsQueryHandler pqh = new PersonsQueryHandler();
                if (opr == 1) {
                    res = pqh.createPerson(variables.get(0), variables.get(1), variables.get(2), variables.get(3));
                } else if (opr == 2) {
                    Person p = pqh.getPersonById(Integer.parseInt(variables.get(0)));
                    resultat = p.toString();
                    res = true;
                } else if (opr == 3) {
                    res = pqh.updatePerson(Integer.parseInt(variables.get(0)), variables.get(1), variables.get(2), variables.get(3), variables.get(4));
                } else if (opr == 4) {
                    res = pqh.deletePerson(Integer.parseInt(variables.get(0)));
                }

            } else if (table == 2) {
                PhoneNumbQueryHandler pnqh = new PhoneNumbQueryHandler();
                if (opr == 1) {
                    res = pnqh.createPhoneNumb(variables.get(0), variables.get(1), variables.get(2));
                } else if (opr == 2) {
                    ArrayList<PhoneNumber> pnrs = pnqh.selectPhoneNumbers(Integer.parseInt(variables.get(0)));
                    StringBuilder sb = new StringBuilder();
                    pnrs.forEach(p -> sb.append(p.getType()).append(" ").append(p.getNumber()).append("\n"));
                    resultat = sb.toString();
                    res = true;
                } else if (opr == 3) {
                    res = pnqh.updatePhoneNumber(variables.get(2), variables.get(1), variables.get(0));
                } else if (opr == 4) {
                    res = pnqh.deletePhoneNumb(variables.get(0));
                }

            } else if (table == 3) {
                EmailQueryHandler eqh = new EmailQueryHandler();
                if (opr == 1) {
                    res = eqh.createEmail(variables.get(0), variables.get(1), variables.get(2));
                } else if (opr == 2) {
                    ArrayList<Email> ems = eqh.selectEmails(Integer.parseInt(variables.get(0)));
                    StringBuilder sb = new StringBuilder();
                    ems.forEach(e -> sb.append(e.getType()).append(" ").append(e.getEmail()).append("\n"));
                    resultat = sb.toString();
                    res = true;
                } else if (opr == 3) {
                    res = eqh.updateEmail(variables.get(2), variables.get(1), variables.get(0));
                } else if (opr == 4) {
                    res = eqh.deleteEmail(variables.get(0));
                }

            } else if (table == 4) {
                RelationshipQueryHandler rqh = new RelationshipQueryHandler();
                if (opr == 1) {
                    RelationshipTypes rt = variables.get(1).equalsIgnoreCase("parent") ? RelationshipTypes.PARENT :
                            variables.get(1).equalsIgnoreCase("child") ? RelationshipTypes.CHILD :
                            variables.get(1).equalsIgnoreCase("sibling") ? RelationshipTypes.SIBLING :
                            RelationshipTypes.SPOUSE;

                    res = rqh.createRelationship(Integer.parseInt(variables.get(0)), rt, Integer.parseInt(variables.get(2)));
                } else if (opr == 2) {
                    ArrayList<Pair<Integer, String>> rel = rqh.selectRelationships(Integer.parseInt(variables.get(0)));
                    StringBuilder sb = new StringBuilder();
                    rel.forEach(e -> {
                        sb.append(e.getKey() + " " + e.getValue()).append("\n");
                    });
                    resultat = sb.toString();
                    res = true;
                } else if (opr == 3) {
                    RelationshipTypes rt = variables.get(1).equalsIgnoreCase("parent") ? RelationshipTypes.PARENT :
                            variables.get(1).equalsIgnoreCase("child") ? RelationshipTypes.CHILD :
                            variables.get(1).equalsIgnoreCase("sibling") ? RelationshipTypes.SIBLING :
                            RelationshipTypes.SPOUSE;

                    res = rqh.updateRelationship(Integer.parseInt(variables.get(0)), rt, Integer.parseInt(variables.get(2)));
                } else if (opr == 4) {
                    res = rqh.deleteRelationship(Integer.parseInt(variables.get(0)), Integer.parseInt(variables.get(1)));
                }

            } else if (table == 5) { // address
                AddressQueryHandler aqh = new AddressQueryHandler();
                if (opr == 1) {
                    res = aqh.insertAddress(variables.get(0));
                } else if (opr == 2) {
                    resultat = aqh.selectAddress(Integer.parseInt(variables.get(0)));
                    res = true;
                } else if (opr == 3) {
                    res = aqh.updateAddress(Integer.parseInt(variables.get(0)), variables.get(1));
                } else if (opr == 4) {
                    res = aqh.deleteAddress(Integer.parseInt(variables.get(0)));
                }

            }

            // write output
            if (opr == 1) {
                if (res == true) {
                    System.out.println("Created");
                } else {
                    System.out.println("Error occurred while creating");
                }
            } else if (opr == 2) {
                if (res == true) {
                    System.out.println(resultat);
                } else {
                    System.out.println("Error occurred while reading");
                }
            } else if (opr == 3) {
                if (res == true) {
                    System.out.println("Updated");
                } else {
                    System.out.println("Error occurred while updating");
                }
            } else if (opr == 4) {
                if (res == true) {
                    System.out.println("Deleted");
                } else {
                    System.out.println("Error occurred while deleting");
                }
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }


    private String mainMenu(Scanner input) {
        StringBuilder sb = new StringBuilder();
        sb.append("1. Search\n").append("2.CRUD operations\n")
                .append("(Write the number of the operation)>");
        System.out.print(sb.toString());
        String operation = input.nextLine();
        return operation;
    }

    private String crudOprMenu(Scanner input) {
        StringBuilder sb = new StringBuilder();
        sb.append("1. Create\n")
                .append("2. Read\n")
                .append("3. Edit\n")
                .append("4. Delete\n")
                .append("(Write the number of the operation)>");
        System.out.println(sb.toString());
        return input.nextLine();
    }

    private String crudMenu(Scanner input) {
        StringBuilder sb = new StringBuilder();
        sb.append("1. Person\n")
                .append("2. Phone numbers\n")
                .append("3. Emails\n")
                .append("4. Relationships\n")
                .append("5. Address\n")
                .append("(Write the number of the operation)>");
        System.out.println(sb.toString());
        return input.nextLine();
    }

    private ArrayList<String> crudMenu2(Scanner input, int crud, int table) {
        String crudString = crud == 1 ? "Create" : crud == 2 ?
                "Read" : crud == 3 ? "Edit" : crud == 4 ? "Delete" : "UNDEFINED",

        tableString = table == 1 ? "Person" : table == 2 ? "Phone numbers" :
                table == 3 ? "Emails" : table == 4 ? "Relationships" :
                table == 5 ? "Address" : "UNDEFINED";

        if (crudString.equalsIgnoreCase("UNDEFINED") || tableString.equalsIgnoreCase("UNDEFINED"))
            return new ArrayList<String>();

        StringBuilder sb = new StringBuilder();
        sb.append(crudString).append(" ").append(tableString).append("\n");
        System.out.println(sb.toString());
        switch (crud) {
            case 1:
                return crudMenu2CreateHelper(table, input);
            case 2:
                ArrayList<String> res = new ArrayList<>();
                res.add(crudMenu2ReadHelper(tableString, input));
                return res;
            case 3:
                return crudMenu2EditHelper(tableString, input);
            case 4:
                ArrayList<String> res2 = crudMenu2DeleteHelper(tableString, input);
                return res2;
            default:
                return new ArrayList<String>();
        }


    }

    private ArrayList<String> crudMenu2CreateHelper(int table, Scanner input) {
        ArrayList<String> resultList = new ArrayList<String>();

        if (table == 1) {
            System.out.print("\nFirst name: >");
            resultList.add(input.nextLine());

            System.out.print("\nLast name: >");
            resultList.add(input.nextLine());

            System.out.print("\nDate of birth: >");
            resultList.add(input.nextLine());

            System.out.print("\nAddress: >");
            resultList.add(input.nextLine());
        } else if (table == 2) {
            System.out.print("\nNumber: >");
            resultList.add(input.nextLine());

            System.out.print("\nType: >");
            resultList.add(input.nextLine());

            System.out.print("\nOwner(ID): >");
            resultList.add(input.nextLine());

        } else if (table == 3) {
            System.out.print("\nEmail: >");
            resultList.add(input.nextLine());

            System.out.print("\nType: >");
            resultList.add(input.nextLine());

            System.out.print("\nOwner(ID): >");
            resultList.add(input.nextLine());
        } else if (table == 4) {
            System.out.print("\nPerson one(ID): >");
            resultList.add(input.nextLine());

            System.out.print("\nIs (Parent || Child || Spouse || Sibling) : >");
            resultList.add(input.nextLine());

            System.out.print("\nto Person two (ID): >");
            resultList.add(input.nextLine());
        } else if(table == 5){
            System.out.print("\nAddress: >");
            resultList.add(input.nextLine());
        }
        return resultList;
    }

    private String crudMenu2ReadHelper(String table, Scanner input) {
        if (table.equalsIgnoreCase("Person")) {
            System.out.print("\nID >");
            return input.nextLine();
        } else if (table.equalsIgnoreCase("Phone numbers")) {
            System.out.print("\nID >");
            return input.nextLine();
        } else if (table.equalsIgnoreCase("Emails")) {
            System.out.print("\nID >");
            return input.nextLine();
        } else if (table.equalsIgnoreCase("Relationships")) {
            System.out.print("\nID >");
            return input.nextLine();
        } else if(table.equalsIgnoreCase("Address")){
            System.out.print("\nID >");
            return input.nextLine();
        }
        return "";
    }

    private ArrayList<String> crudMenu2EditHelper(String table, Scanner input) {
        ArrayList<String> resultList = new ArrayList<>();

        if (table.equalsIgnoreCase("Person")) {
            System.out.print("\nID: >");
            resultList.add(input.nextLine());

            System.out.print("\nFirst name: >");
            resultList.add(input.nextLine());

            System.out.print("\nLast name: >");
            resultList.add(input.nextLine());

            System.out.print("\nDate of birth: >");
            resultList.add(input.nextLine());

            System.out.print("\nAddress(ID): >");
            resultList.add(input.nextLine());

        } else if (table.equalsIgnoreCase("Phone numbers")) {
            System.out.print("\nOld Number: >");
            resultList.add(input.nextLine());

            System.out.print("\nType: >");
            resultList.add(input.nextLine());

            System.out.print("\nNew Number: >");
            resultList.add(input.nextLine());

        } else if (table.equalsIgnoreCase("Emails")) {
            System.out.print("\nOld Email: >");
            resultList.add(input.nextLine());

            System.out.print("\nType: >");
            resultList.add(input.nextLine());

            System.out.print("\nNew Email: >");
            resultList.add(input.nextLine());
        } else if (table.equalsIgnoreCase("Relationships")) {
            System.out.print("\nPerson one(ID): >");
            resultList.add(input.nextLine());

            System.out.print("\nIs (Parent || Child || Spouse || Sibling) : >");
            resultList.add(input.nextLine());

            System.out.print("\nto Person two (ID): >");
            resultList.add(input.nextLine());
        } else if(table.equalsIgnoreCase("Address")){
            System.out.print("\nID >");
            resultList.add(input.nextLine());

            System.out.print("\nNew address >");
            resultList.add(input.nextLine());
        }
        return resultList;
    }

    private ArrayList<String> crudMenu2DeleteHelper(String table, Scanner input) {
        ArrayList<String> resultList = new ArrayList<>();
        if (table.equalsIgnoreCase("Person")) {
            System.out.print("\nID: >");
        } else if (table.equalsIgnoreCase("Phone numbers")) {
            System.out.print("\nNumber: >");
        } else if (table.equalsIgnoreCase("Emails")) {
            System.out.print("\nEmail: >");
        } else if (table.equalsIgnoreCase("Relationships")) {
            System.out.print("\nPerson one(ID): >");
            resultList.add(input.nextLine());
            System.out.print("\nPerson two(ID): >");
        } else if (table.equalsIgnoreCase("Address")){
            System.out.println("\nAddress: >");
        }
        resultList.add(input.nextLine());
        return resultList;
    }

    private String searchMenu(Scanner input) {
        StringBuilder sb = new StringBuilder();
        sb.append("\nSearch:>");
        System.out.print(sb.toString());
        return input.nextLine();
    }
}

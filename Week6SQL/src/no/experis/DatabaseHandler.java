package no.experis;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DatabaseHandler {
    private static final String URL = "jdbc:sqlite:resources\\Task17.db";

    public static void closeConnection(Connection conn) {
        try {
            if (conn != null)
                conn.close();
        }catch (SQLException ex){
            ex.printStackTrace();
        }
    }

    public static Connection getConnection() throws Exception{
        Connection conn;
        conn = DriverManager.getConnection(URL);
        return conn;
    }

    public static boolean rollBackSafe(Connection conn, PreparedStatement pstmt, PreparedStatement pstmt2){
        boolean isSuccess = false;
        try {
            boolean autoCommit = conn.getAutoCommit();
            try {
                conn.setAutoCommit(false);
                pstmt.executeUpdate();
                pstmt2.executeUpdate();
                conn.commit();
                System.out.println("Transaction successful.");
                isSuccess = true;
            } catch (SQLException ex) {
                conn.rollback();
                System.out.println("Transaction failed.");
                System.out.println(ex.getMessage());
            } finally {
                conn.setAutoCommit(autoCommit);
            }
        }catch(SQLException ex){
            System.out.println("Transaction failed.");
        }
        return isSuccess;
    }
}

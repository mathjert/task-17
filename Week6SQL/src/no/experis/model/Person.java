package no.experis.model;

import javafx.util.Pair;
import no.experis.querys.PersonsQueryHandler;

import java.util.ArrayList;

public class Person {
    private int id;
    private String firstName;
    private String lastName;
    private String address;
    private String dateOfBirth;
    private ArrayList<PhoneNumber> phoneNumbers = new ArrayList<>();
    private ArrayList<Email> emails = new ArrayList<>();
    private ArrayList<Pair<Integer, String>> relationships = new ArrayList<>();

    public Person(int id, String firstName, String lastName, String address, String dateOfBirth) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.dateOfBirth = dateOfBirth;
    }

    public int getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public ArrayList<PhoneNumber> getPhoneNumbers() {
        return phoneNumbers;
    }

    public ArrayList<Email> getEmails() {
        return emails;
    }

    public ArrayList<Pair<Integer, String>> getRelationships() {
        return relationships;
    }

    // Setters

    public void setPhoneNumbers(ArrayList<PhoneNumber> numbers) {
        this.phoneNumbers.addAll(numbers);
    }

    public void setEmails(ArrayList<Email> emails) {
        this.emails.addAll(emails);
    }

    public void setRelationships(ArrayList<Pair<Integer, String>> relationship) {
        this.relationships.addAll(relationship);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        String $oneIndent = "     ";

        sb.append("Name: ").append(firstName).append(" ").append(lastName).append("\n")
                .append("Address: ").append(address).append("\n")
                .append("Born: ").append(dateOfBirth).append("\n");

        sb.append("Phone numbers: \n");
        for (PhoneNumber p : phoneNumbers) {
            sb.append($oneIndent).append(p.getType()).append(" ").append(p.getNumber()).append("\n");
        }

        sb.append("Emails: \n");
        for (Email e : emails) {
            sb.append($oneIndent).append(e.getType()).append(" ").append(e.getEmail()).append("\n");
        }

        sb.append("Relationships: \n");
        try {
            PersonsQueryHandler pqh = new PersonsQueryHandler();
            for (Pair<Integer, String> relationship : relationships) {
                Person p = pqh.getPersonById(relationship.getKey());
                sb.append($oneIndent).append(relationship.getValue() + " of ").append(p.getFirstName()).append(" ").append(p.getLastName()).append("\n");
            }
        } catch (Exception e) {
            sb.append("\n\nError");
        }

        return sb.toString();
    }
}

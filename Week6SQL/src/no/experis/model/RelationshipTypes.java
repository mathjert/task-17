package no.experis.model;

public enum RelationshipTypes {

    PARENT("Parent", "Child"),
    CHILD("Child", "Parent"),
    SPOUSE("Spouse", "Spouse"),
    SIBLING("Sibling", "Sibling");

    private String type;
    private String reverseType;

    private RelationshipTypes(String type, String reverseType) {
        this.type = type;
        this.reverseType = reverseType;
    }

    public String toString() {
        return this.type;
    }

    public String reverse() {
        return this.reverseType;
    }
}

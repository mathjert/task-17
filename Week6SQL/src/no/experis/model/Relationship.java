package no.experis.model;

public class Relationship {
    private int Person1;
    private int Person2;
    private String type;

    public Relationship(int ID1, String type, int ID2) {
        this.Person1 = ID1;
        this.Person2 = ID2;
        this.type = type;
    }

    public int getPerson1() {
        return Person1;
    }

    public int getPerson2() {
        return Person2;
    }

    public String getType() {
        return type;
    }
}

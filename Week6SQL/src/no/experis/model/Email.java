package no.experis.model;

public class Email {
    private int id;
    private String email;
    private String type;

    public Email(int id, String email, String type) {
        this.id = id;
        this.email = email;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getType() {
        return type;
    }
}

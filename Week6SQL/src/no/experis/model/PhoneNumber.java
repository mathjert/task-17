package no.experis.model;

public class PhoneNumber {
    private int id;
    private String number;
    private String type;

    public PhoneNumber(int id, String number, String type) {
        this.id = id;
        this.number = number;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public String getNumber() {
        return number;
    }

    public String getType() {
        return type;
    }
}

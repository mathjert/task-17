package no.experis;

import no.experis.model.Person;
import no.experis.querys.SearchQueryHandler;
import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

public class Main {

    public static JSONArray convertToJSON(ResultSet rs) throws SQLException {
        JSONArray json = new JSONArray();
        ResultSetMetaData rsmd = rs.getMetaData();
        while (rs.next()) {
            int numColumns = rsmd.getColumnCount();
            JSONObject obj = new JSONObject();
            for (int i = 1; i <= numColumns; i++) {
                String column_name = rsmd.getColumnLabel(i);
                obj.put(column_name, rs.getObject(column_name));
            }
            json.put(obj);
        }
        return json;
    }

    public static void main(String[] args) {
        try {
            Menu m = new Menu();
            while(true){
                m.printMenu();
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println("Exeption i main");
        }
    }
}

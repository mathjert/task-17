package no.experis.querys;

import javafx.util.Pair;
import no.experis.DatabaseHandler;
import no.experis.model.RelationshipTypes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import static no.experis.querys.PersonsQueryHandler.isPersonInDatabase;

public class RelationshipQueryHandler {

    public ArrayList<Pair<Integer, String>> selectRelationships(int ID) throws Exception {
        ArrayList<Pair<Integer, String>> relationships = new ArrayList<>();
        Connection conn = DatabaseHandler.getConnection();
        String sqlQuery = "SELECT * FROM Relationships WHERE Person1=?";
        PreparedStatement pstmt = conn.prepareStatement(sqlQuery);
        pstmt.setInt(1, ID);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            Pair<Integer, String> pair = new Pair(rs.getInt("Person2"), rs.getString("Type"));
            relationships.add(pair);
        }
        DatabaseHandler.closeConnection(conn);
        return relationships;
    }

    public boolean createRelationship(int ID1, RelationshipTypes type, int ID2) throws Exception {
        boolean isSuccess = false;
        Connection conn = DatabaseHandler.getConnection();

        if ((ID1 != ID2) && (isPersonInDatabase(ID1) && isPersonInDatabase(ID2))) {

            ArrayList<Pair<Integer, String>> result = selectRelationships(ID1);
            for (Pair<Integer, String> p : result) {
                if(p.getKey() == ID2){
                    DatabaseHandler.closeConnection(conn);
                    return false; // Hvis vi sier at et par kun kan ha et type forhold
                }
            }

            String sqlQuery = "INSERT INTO Relationships (Person1, Type, Person2) VALUES (?, ?, ?) ";
            PreparedStatement pstmt = conn.prepareStatement(sqlQuery);
            pstmt.setInt(1, ID1);
            pstmt.setString(2, type.toString());
            pstmt.setInt(3, ID2);

            String sqlQuery2 = "INSERT INTO Relationships (Person1, Type, Person2) VALUES (?, ?, ?) ";
            PreparedStatement pstmt2 = conn.prepareStatement(sqlQuery2);
            pstmt2.setInt(1, ID2);
            pstmt2.setString(2, type.reverse());
            pstmt2.setInt(3, ID1);

            isSuccess = DatabaseHandler.rollBackSafe(conn, pstmt, pstmt2);

        } else {
            System.out.println("You must add two people to the database before you can add a relationship.");
        }

        DatabaseHandler.closeConnection(conn);
        return isSuccess;
    }

    public boolean updateRelationship(int ID1, RelationshipTypes type, int ID2) throws Exception {
        boolean isSuccess;
        Connection conn = DatabaseHandler.getConnection();

        String sqlQuery = "UPDATE Relationships SET Type=? WHERE Person1=? AND Person2=?";
        PreparedStatement pstmt = conn.prepareStatement(sqlQuery);
        pstmt.setString(1, type.toString());
        pstmt.setInt(2, ID1);
        pstmt.setInt(3, ID2);

        String sqlQuery2 = "UPDATE Relationships SET Type=? WHERE Person1=? AND Person2=?";
        PreparedStatement pstmt2 = conn.prepareStatement(sqlQuery2);
        pstmt2.setString(1, type.reverse());
        pstmt2.setInt(2, ID2);
        pstmt2.setInt(3, ID1);

        isSuccess = DatabaseHandler.rollBackSafe(conn, pstmt, pstmt2);
        DatabaseHandler.closeConnection(conn);
        return isSuccess;
    }

    public boolean deleteRelationship(int ID1, int ID2) throws Exception {
        boolean isSuccess;
        Connection conn = DatabaseHandler.getConnection();

        String sqlQuery = "DELETE FROM Relationships WHERE Person1=? AND Person2=?";
        PreparedStatement pstmt = conn.prepareStatement(sqlQuery);
        pstmt.setInt(1, ID1);
        pstmt.setInt(2, ID2);

        String sqlQuery2 = "DELETE FROM Relationships WHERE Person2=? AND Person1=?";
        PreparedStatement pstmt2 = conn.prepareStatement(sqlQuery2);
        pstmt2.setInt(1, ID1);
        pstmt2.setInt(2, ID2);

        isSuccess = DatabaseHandler.rollBackSafe(conn, pstmt, pstmt2);
        DatabaseHandler.closeConnection(conn);
        return isSuccess;
    }
}

package no.experis.querys;

import no.experis.DatabaseHandler;

import java.sql.*;
import java.util.ArrayList;

public class SearchQueryHandler {
    DatabaseHandler dbh;

    public ArrayList<Integer> search(String search) {
        Connection conn = null;
        ArrayList<Integer> personsId = new ArrayList<Integer>();
        try {
            conn = DatabaseHandler.getConnection();
            String sqlQuery =
                    " SELECT DISTINCT ID" +
                            " FROM Persons" +
                            " LEFT JOIN PhoneNumbers ON Persons.ID=PhoneNumbers.Owner" +
                            " WHERE PhoneNumbers.PhoneNumber like ?" +
                            " OR Persons.FirstName||' '||Persons.LastName like ?";

            PreparedStatement preStmt = conn.prepareStatement(sqlQuery);
            preStmt.setString(1, "%" + search + "%");
            preStmt.setString(2, "%" + search + "%");
            ResultSet rs = preStmt.executeQuery();

            while (rs.next()) {
                personsId.add((rs.getInt("ID")));
            }
            return personsId;
        } catch (Exception exc) {
            System.out.println("Error: " + exc);
            return null;
        } finally {
            DatabaseHandler.closeConnection(conn);
        }
    }

}

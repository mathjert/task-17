package no.experis.querys;

import jdk.nashorn.internal.runtime.ECMAException;
import no.experis.DatabaseHandler;
import no.experis.model.Person;

import javax.xml.crypto.Data;
import java.sql.*;
import java.util.ArrayList;

public class PersonsQueryHandler {

    public static boolean isPersonInDatabase(int id) throws Exception {
        boolean isPersonInDatabase;
        Connection conn = DatabaseHandler.getConnection();
        String querySelect = "SELECT * FROM Persons WHERE ID=?";
        PreparedStatement preStmt = conn.prepareStatement(querySelect);
        preStmt.setInt(1, id);
        ResultSet rs = preStmt.executeQuery();
        isPersonInDatabase = rs.next();
        DatabaseHandler.closeConnection(conn);
        return isPersonInDatabase;
    }

    public Person getPersonById(int id) throws Exception {
        Person p = null;
        Connection conn = DatabaseHandler.getConnection();

        String querySelect = "SELECT * FROM Persons WHERE ID=?";
        PreparedStatement preStmt = conn.prepareStatement(querySelect);
        preStmt.setInt(1, id);
        ResultSet rs = preStmt.executeQuery();

        if(!rs.next()){
            DatabaseHandler.closeConnection(conn);
            throw new Exception("Person not found with id: " + id);
        }

        int addressID = rs.getInt("Address");
        String querySelectAddr = "SELECT * FROM Addresses WHERE ID=?";
        PreparedStatement preStmtAddr = conn.prepareStatement(querySelectAddr);
        preStmtAddr.setInt(1, addressID);
        ResultSet rsAddr = preStmtAddr.executeQuery();

        if(!rsAddr.next()){
            DatabaseHandler.closeConnection(conn);
            throw new Exception("Address not found for person with id: " + id);
        }

        p = new Person(
                rs.getInt("ID"),
                rs.getString("FirstName"),
                rs.getString("LastName"),
                rsAddr.getString("Address"),
                rs.getString("DateOfBirth")
        );
        DatabaseHandler.closeConnection(conn);
        return p;
    }


    public boolean createPerson(String firstName, String lastName, String dateOfBirth, String address) throws Exception {
        Connection conn = null;
        try { // try due to rollback possibilities
            // Establishing connection to the database
            conn = DatabaseHandler.getConnection();

            // preparing address query
            String findAddressQuery = "Select ID from Addresses WHERE Address=?";
            PreparedStatement preStmt = conn.prepareStatement(findAddressQuery);
            preStmt.setString(1, address);
            ResultSet rs = preStmt.executeQuery();

            int addressID = 0; // this will be the address put into our new person
            if (!rs.next()) { // if ResultSet har noe
                // create entry
                String addressQuery = "INSERT INTO Addresses (Address) VALUES (?)";
                PreparedStatement preStmt3 = conn.prepareStatement(addressQuery);
                preStmt3.setString(1, address);
                int tmp = preStmt3.executeUpdate();
                if(tmp > 0){
                    // Selecting the address since executeUpdate does not return
                    preStmt3 = conn.prepareStatement("SELECT ID FROM Addresses WHERE Address = ?");
                    preStmt3.setString(1, address);
                    ResultSet rs2 = preStmt3.executeQuery();
                    addressID = rs2.getInt("ID");
                }
            } else {
                addressID = rs.getInt("ID"); // if exist
            }
            //preparing insert query
            String insertQuery = "INSERT INTO Persons (FirstName, LastName, DateOfBirth, Address) VALUES (?, ?, ?, ?)";
            PreparedStatement preStmt2 = conn.prepareStatement(insertQuery);
            preStmt2.setString(1, firstName);
            preStmt2.setString(2, lastName);
            preStmt2.setString(3, dateOfBirth);
            preStmt2.setInt(4, addressID);

            preStmt2.executeUpdate(); // execute query

        } catch (SQLException e) {
            try {
                if (conn != null)
                    conn.rollback();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                return false;
            }
            System.out.println("ERROR " + e.getMessage());
            return false;
        } catch (Exception e) {
            try {
                if (conn != null)
                    conn.rollback();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                return false;
            }
            System.out.println("ERROR2 " + e.getMessage());
            return false;
        } finally {
            DatabaseHandler.closeConnection(conn);
        }
        return true;
    }

    public boolean updatePerson(int id, String firstName, String lastName, String dob, String address) throws Exception {
        Connection conn = DatabaseHandler.getConnection();

        String selectQuery = "SELECT * from Persons WHERE ID = ?";
        PreparedStatement preStmt = conn.prepareStatement(selectQuery);
        preStmt.setInt(1, id);
        ResultSet rs = preStmt.executeQuery();

        if (!rs.next()) {
            System.out.println("No person with given ID");
            return false;
        }

        String updateQuery = "UPDATE Persons SET FirstName=?, LastName=?, DateOfBirth=?, Address=? WHERE id = ?";
        PreparedStatement preStmt2 = conn.prepareStatement(updateQuery);
        preStmt2.setString(1, firstName);
        preStmt2.setString(2, lastName);
        preStmt2.setString(3, dob);
        preStmt2.setString(4, address);
        preStmt2.setInt(5, id);

        int result = preStmt2.executeUpdate();
        DatabaseHandler.closeConnection(conn);
        if (result > 0)
            return true;

        return false;
    }


    public boolean deletePerson(int id) throws Exception {
        Connection conn = DatabaseHandler.getConnection();
        String deleteQuery = "DELETE FROM Persons WHERE ID = ?";
        PreparedStatement preStmt2 = conn.prepareStatement(deleteQuery);
        preStmt2.setInt(1, id);
        int result = preStmt2.executeUpdate();
        if (result == 1)
            return true;
        return false;
    }

}

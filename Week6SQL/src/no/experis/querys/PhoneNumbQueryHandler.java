package no.experis.querys;

import no.experis.DatabaseHandler;
import no.experis.model.PhoneNumber;

import java.sql.*;
import java.util.ArrayList;

public class PhoneNumbQueryHandler {

    public ArrayList<PhoneNumber> selectPhoneNumbers(int ID) throws Exception {
        ArrayList<PhoneNumber> phoneNumbers = new ArrayList<>();
        if (PersonsQueryHandler.isPersonInDatabase(ID)) {
            Connection conn = DatabaseHandler.getConnection();
            String allPhoneNumbers = "SELECT * FROM PhoneNumbers WHERE Owner=?";
            PreparedStatement pstmt = conn.prepareStatement(allPhoneNumbers);
            pstmt.setInt(1, ID);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                PhoneNumber phoneNumber = new PhoneNumber(
                        rs.getInt("Owner"),
                        rs.getString("PhoneNumber"),
                        rs.getString("Type"));
                phoneNumbers.add(phoneNumber);
            }
            DatabaseHandler.closeConnection(conn);
        }
        return phoneNumbers;
    }

    public boolean createPhoneNumb(String numb, String type, String owner) throws Exception {

        if (PersonsQueryHandler.isPersonInDatabase(Integer.parseInt(owner))) {
            System.out.println("person is");
            Connection conn = DatabaseHandler.getConnection();
            String createPhoneNumbSql = "INSERT INTO PhoneNumbers (PhoneNumber, Type, Owner) VALUES (?, ?, ?)";
            PreparedStatement pstmt = conn.prepareStatement(createPhoneNumbSql);
            pstmt.setString(1, numb);
            pstmt.setString(2, type);
            pstmt.setString(3, owner);

            int result = pstmt.executeUpdate();
            DatabaseHandler.closeConnection(conn);
            if (result == 1) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    public boolean deletePhoneNumb(String phoneNumb) throws Exception {
        Connection conn = DatabaseHandler.getConnection();
        String deletePhoneSql = "DELETE FROM PhoneNumbers WHERE PhoneNumber=?";
        PreparedStatement pstmt = conn.prepareStatement(deletePhoneSql);
        pstmt.setString(1, phoneNumb);

        int result = pstmt.executeUpdate();
        DatabaseHandler.closeConnection(conn);
        if (result == 1) {
            return true;
        } else {
            return false;
        }
    }

    public boolean updatePhoneNumber(String newPhoneNumb, String newType, String oldNumber) throws Exception {
        Connection conn = DatabaseHandler.getConnection();
        String updateEmailSql = "UPDATE PhoneNumbers SET PhoneNumber=?, Type=? WHERE PhoneNumber=?";
        PreparedStatement pstmt = conn.prepareStatement(updateEmailSql);
        pstmt.setString(1, newPhoneNumb);
        pstmt.setString(2, newType);
        pstmt.setString(3, oldNumber);

        int result = pstmt.executeUpdate();
        DatabaseHandler.closeConnection(conn);
        if (result == 1) {
            return true;
        } else {
            return false;
        }
    }
}

package no.experis.querys;

import no.experis.DatabaseHandler;
import no.experis.model.Email;

import java.sql.*;
import java.util.ArrayList;

public class EmailQueryHandler {

    public ArrayList<Email> selectEmails(int ID) throws Exception {
        ArrayList<Email> emails = new ArrayList<>();

        if (PersonsQueryHandler.isPersonInDatabase(ID)) {
            Connection conn = DatabaseHandler.getConnection();
            String allEmails = "SELECT * FROM Emails WHERE Owner=?";
            PreparedStatement pstmt = conn.prepareStatement(allEmails);
            pstmt.setInt(1, ID);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                Email email = new Email(
                        rs.getInt("Owner"),
                        rs.getString("EmailAddress"),
                        rs.getString("Type"));
                emails.add(email);
            }
            DatabaseHandler.closeConnection(conn);
        }
        return emails;
    }

    public boolean createEmail(String email, String type, String owner) throws Exception {
        if (PersonsQueryHandler.isPersonInDatabase(Integer.parseInt(owner))) {
            Connection conn = DatabaseHandler.getConnection();
            String createEmailSql = "INSERT INTO Emails (EmailAddress, Type, Owner) VALUES (?, ?, ?)";
            PreparedStatement pstmt = conn.prepareStatement(createEmailSql);
            pstmt.setString(1, email);
            pstmt.setString(2, type);
            pstmt.setString(3, owner);

            int result = pstmt.executeUpdate();
            DatabaseHandler.closeConnection(conn);
            if (result == 1) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public boolean deleteEmail(String email) throws Exception {
        Connection conn = DatabaseHandler.getConnection();
        String deleteEmailSql = "DELETE FROM Emails WHERE EmailAddress=?";
        PreparedStatement pstmt = conn.prepareStatement(deleteEmailSql);
        pstmt.setString(1, email);

        int result = pstmt.executeUpdate();
        DatabaseHandler.closeConnection(conn);
        if (result == 1) {
            return true;
        } else {
            return false;
        }
    }

    public boolean updateEmail(String newEmail, String newType, String oldEmail) throws Exception {
        Connection conn = DatabaseHandler.getConnection();
        String updateEmailSql = "UPDATE Emails SET EmailAddress=?, Type=? WHERE EmailAddress=?";
        PreparedStatement pstmt = conn.prepareStatement(updateEmailSql);
        pstmt.setString(1, newEmail);
        pstmt.setString(2, newType);
        pstmt.setString(3, oldEmail);

        int result = pstmt.executeUpdate();
        DatabaseHandler.closeConnection(conn);
        if (result == 1) {
            return true;
        } else {
            return false;
        }
    }
}

package no.experis.querys;

import no.experis.DatabaseHandler;

import java.sql.*;

public class AddressQueryHandler {

    public String selectAddress(int ID) throws Exception {
        Connection conn = DatabaseHandler.getConnection();

        String sqlQuery = "SELECT * FROM Addresses WHERE ID=?";
        PreparedStatement pstmt = conn.prepareStatement(sqlQuery);
        pstmt.setInt(1, ID);
        ResultSet rs = pstmt.executeQuery();
        StringBuilder sb = new StringBuilder();
        while (rs.next()) {
            sb.append(rs.getString("Address")).append("\n");
        }

        DatabaseHandler.closeConnection(conn);
        return sb.toString();
    }

    public boolean insertAddress(String address) throws Exception {
        Connection conn = DatabaseHandler.getConnection();

        String sqlCheck = "SELECT * from Addresses WHERE Address=?"; // see if it exists
        PreparedStatement preStmt = conn.prepareStatement(sqlCheck);
        preStmt.setString(1, address);
        ResultSet rs = preStmt.executeQuery();
        if(rs.next()){ // if it has a next, it returned a line
            DatabaseHandler.closeConnection(conn);
            return false;
        }

        String sqlQuery = "INSERT INTO Addresses (Address) VALUES (?) ";
        PreparedStatement pstmt = conn.prepareStatement(sqlQuery);
        pstmt.setString(1, address);
        int res = pstmt.executeUpdate();
        DatabaseHandler.closeConnection(conn);
        if (res > 0)
            return true;

        return false;
    }

    public boolean updateAddress(int ID, String address) throws Exception {
        Connection conn = DatabaseHandler.getConnection();
        String sqlQuery = "UPDATE Addresses SET Address=? WHERE ID=?";
        PreparedStatement pstmt = conn.prepareStatement(sqlQuery);
        pstmt.setString(1, address);
        pstmt.setInt(2, ID);
        int res = pstmt.executeUpdate();
        DatabaseHandler.closeConnection(conn);
        if (res > 0)
            return true;

        return false;
    }

    public boolean deleteAddress(int ID) throws Exception {
        Connection conn = DatabaseHandler.getConnection();
        String sqlQuery = "DELETE FROM Addresses WHERE ID=?";
        PreparedStatement pstmt = conn.prepareStatement(sqlQuery);
        pstmt.setInt(1, ID);
        int res = pstmt.executeUpdate();
        DatabaseHandler.closeConnection(conn);
        if (res > 0)
            return true;

        return false;
    }
}
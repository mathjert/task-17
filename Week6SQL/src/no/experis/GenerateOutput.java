package no.experis;

import no.experis.model.Person;
import no.experis.querys.EmailQueryHandler;
import no.experis.querys.PersonsQueryHandler;
import no.experis.querys.PhoneNumbQueryHandler;
import no.experis.querys.RelationshipQueryHandler;

import java.util.ArrayList;

public class GenerateOutput {
    private PersonsQueryHandler pqh;
    private EmailQueryHandler eqh;
    private PhoneNumbQueryHandler pnqh;
    private RelationshipQueryHandler rqh;

    public GenerateOutput() {
        pqh = new PersonsQueryHandler();
        eqh = new EmailQueryHandler();
        pnqh = new PhoneNumbQueryHandler();
        rqh = new RelationshipQueryHandler();
    }

    public ArrayList<Person> generateMatchList(ArrayList<Integer> IDS) {
        ArrayList<Person> resultList = new ArrayList<>();
        for (int id : IDS) {
            resultList.add(generatePerson(id));
        }
        return resultList;
    }

    private Person generatePerson(int id) {
        Person p = null;
        try {
            p = pqh.getPersonById(id);
            p.setEmails(eqh.selectEmails(id));
            p.setPhoneNumbers(pnqh.selectPhoneNumbers(id));
            p.setRelationships(rqh.selectRelationships(id));
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return p;
        }
        return p;
    }
}
